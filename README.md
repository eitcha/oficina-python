Oficina de Introdução à Programação Usando Python
===========================

Oficina da [EITCHA](http://eitcha.org), voltada para pessoas que não sabem programar mas abrangendo tópicos interessantes até para quem tem algum conhecimento em programação.
Atualmente o conteúdo encontra-se dividido em duas partes com previsão de 4h de oficina.

O objetivo desse material é apresentar uma oficina completa e auto-contida, que não precise de muitos outros recursos para funcionar.

### Bibliotecas utilizadas: ###

* Requests
* lxml

* No lugar da lxml você pode utilizar a [BeautifulSoup](https://wiki.python.org/moin/beautiful%20soup). Claro, a sintaxe vai ser diferente, mas é a mesma ideia.

_OBS: Para a parte II, é necessário acesso à internet._

Conteúdo
==============

### Parte I 2h ###

Programando em Python.

* O que é programar? Quais são as linguagens?
* Por que Python?
* Interagindo com exemplo:
** O que é um algoritmo?
** Tipos: Inteiros, floats, strings, booleanos;
** Operações básicas;
** Dicas de boas práticas 1: nomes de variáveis, comentários;
** Recipientes: listas, tuplas, dicionários;
** Laços e condicionais: for, if, else;
** Definindo funções.


* Mão-na-massa: Construindo meu primeiro programa em Python - FIZZBUZZ (também chamado de "EITCHA LELE").

### Parte II 1h30 ###

Web scraping.

* Mas o que é Web Scraping?
É a obtenção de dados da internet. Ah, então queremos escrever um programa que saiba navegar na internet!

* Mas pra que isso pode ser útil?
Toda a informação que podemos acessar em um site pode ser obtida também pelo nosso programa.

- Pesquisas de mercado
- Opiniões sobre produtos
- Assuntos correntes
- Obtenção de emails
- Agregadores (hotéis, vôos, sites, notícias)
- Ficar "de olho" (crimes, assuntos, padrões)

* HTML
* Funcionamento básico da internet


Saiba mais
============

* Dicas de web scraping do [Brody](https://blog.hartleybrody.com/web-scraping/) [em inglês].
* Exercícios de Python da [Michele](http://www.practicepython.org/) [em inglês] - Confira o ex. 17.
* [O Guia do Mochileiro de Python](http://docs.python-guide.org/en/latest/) [em inglês].
* Aprenda mais sobre XPath na [W3Schools](https://www.w3schools.com/xml/xpath_intro.asp) [em inglês].
* Resumão da internet no [How Stuff Works](https://computer.howstuffworks.com/internet/basics/internet-infrastructure.htm) [Em inglês].
